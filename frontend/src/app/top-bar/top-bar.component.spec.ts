import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { MatDialogModule } from "@angular/material/dialog";
import { TopBarComponent } from "./top-bar.component";
import { UserService } from "../user.service";

describe("TopBarComponent", () => {
  let component: TopBarComponent;
  let fixture: ComponentFixture<TopBarComponent>;

  class MockUserService extends UserService {
    isLoggedIn() {
      return false;
    }
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TopBarComponent],
      providers: [{ provide: UserService, useClass: MockUserService }],
      imports: [RouterModule, HttpClientModule, MatDialogModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it("should clear local storage when onClick is fired", () => {
  //   let service = fixture.debugElement.injector.get(UserService);
  //   spyOn(service, "logout").and.returnValue(false);
  // });
});
