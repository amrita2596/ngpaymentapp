import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { StorageServiceModule } from "ngx-webstorage-service";
import { UserService } from "./user.service";
import { routing } from "./app.routing";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatDialogModule } from "@angular/material/dialog";
import { JwtModule } from "@auth0/angular-jwt";

import { AppComponent } from "./app.component";
import { RegisterComponent } from "./register/register.component";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { TransactionsComponent } from "./transactions/transactions.component";
import { WalletComponent } from "./wallet/wallet.component";
import { AddMoneyComponent } from "./add-money/add-money.component";
import { PayMoneyComponent } from "./pay-money/pay-money.component";
import { TopBarComponent } from "./top-bar/top-bar.component";
import { MatConfirmDialogComponent } from "./mat-confirm-dialog/mat-confirm-dialog.component";
import { MatNotifyDialogComponent } from "./mat-notify-dialog/mat-notify-dialog.component";
import { AuthGuard } from "./auth.guard";

export function tokenGetter() {
  return localStorage.getItem("access_token");
}

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    TransactionsComponent,
    WalletComponent,
    AddMoneyComponent,
    PayMoneyComponent,
    TopBarComponent,
    MatConfirmDialogComponent,
    MatNotifyDialogComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    routing,
    StorageServiceModule,
    BrowserAnimationsModule,
    MatDialogModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ["localhost:4000"],
        blacklistedRoutes: ["localhost:4000/app/login"]
      }
    })
  ],
  providers: [UserService, AuthGuard],
  bootstrap: [AppComponent],
  entryComponents: [
    RegisterComponent,
    MatConfirmDialogComponent,
    MatNotifyDialogComponent
  ]
})
export class AppModule {}
