import { Component, OnInit, Inject } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { UserService } from "../user.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.show();
  }
}
