import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../user.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  user: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.hide();
    this.registerForm = this.formBuilder.group({
      Name: ["", Validators.required],
      Email: ["", [Validators.required, Validators.email]],
      MobileNumber: [
        "",
        [
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(10)
        ]
      ],
      Password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      this.loading = false;
      return;
    }

    this.loading = true;
    this.userService.register(this.registerForm.value).subscribe(
      data => {
        this.user = data;
        this.userService
          .openNotifyDialog("Registration successful !")
          .afterClosed();
        this.router.navigate(["/app/login"]);
      },
      error => {
        this.userService
          .openNotifyDialog("Registration failed !")
          .afterClosed();

        this.loading = false;
        this.registerForm.reset();
      }
    );
  }
}
