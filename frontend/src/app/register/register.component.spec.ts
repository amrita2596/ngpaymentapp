import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { UserService } from "../user.service";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { RegisterComponent } from "./register.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { MatDialogModule } from "@angular/material/dialog";
import { User } from "../models/user";

describe("Register service", () => {
  let userService: UserService;
  let httpMock: HttpTestingController;
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        MatDialogModule
      ],
      declarations: [RegisterComponent],
      providers: [UserService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UserService);
    httpMock = TestBed.get(HttpTestingController);
    fixture.detectChanges();
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should create the app", () => {
    expect(component).toBeDefined();
  });

  it("should create the register service", () => {
    expect(userService.register).toBeTruthy();
  });

  it("should have <h2> with 'Register to Epay'", () => {
    const registerElement: HTMLElement = fixture.nativeElement;
    const h2 = registerElement.querySelector("h2");
    expect(h2.textContent).toEqual("Register to Epay");
  });

  it("should be true if form is valid", () => {
    component.registerForm.controls["Name"].setValue("text");
    component.registerForm.controls["Email"].setValue("text@gmail.com");
    component.registerForm.controls["MobileNumber"].setValue("2435405661");
    component.registerForm.controls["Password"].setValue("text123");
    expect(component.registerForm.valid).toBeTruthy();
  });

  it("should post user to the api via post method", () => {
    const userStub: User = {
      Name: "string",
      Email: "string",
      MobileNumber: 127912873,
      Password: "string"
    };

    const resStub = {
      Name: "string",
      Email: "string",
      MobileNumber: 127912873,
      Password: "string"
    };

    userService.register(userStub).subscribe(res => {
      expect(res).toEqual(resStub);
    });
    const req = httpMock.expectOne("http://localhost:4000/app/register");
    expect(req.request.method).toBe("POST");
    req.flush(userStub);
  });
});
