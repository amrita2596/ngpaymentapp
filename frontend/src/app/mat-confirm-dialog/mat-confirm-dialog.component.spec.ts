import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { MatDialogModule } from "@angular/material/dialog";
import { MatConfirmDialogComponent } from "./mat-confirm-dialog.component";
import { NgModule, Component } from "@angular/core";
import { MatDialog } from "@angular/material";
import { OverlayContainer } from "@angular/cdk/overlay";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@Component({
  template: ""
})
class NoopComponent {}

const TEST_DIRECTIVES = [MatConfirmDialogComponent, NoopComponent];
@NgModule({
  imports: [MatDialogModule, BrowserAnimationsModule],
  exports: TEST_DIRECTIVES,
  declarations: TEST_DIRECTIVES,
  entryComponents: [MatConfirmDialogComponent]
})
class DialogTestModule {}

describe("MatConfirmDialog", () => {
  let dialog: MatDialog;
  let overlayContainerElement: HTMLElement;
  let noop: ComponentFixture<NoopComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DialogTestModule],
      providers: [
        {
          provide: OverlayContainer,
          useFactory: () => {
            overlayContainerElement = document.createElement("div");
            return { getContainerElement: () => overlayContainerElement };
          }
        }
      ]
    });

    dialog = TestBed.get(MatDialog);
    noop = TestBed.createComponent(NoopComponent);
  });

  it("shows confirmation message", () => {
    const config = {
      data: {
        message: "Are you sure you want to continue?"
      }
    };
    dialog.open(MatConfirmDialogComponent, config);
    noop.detectChanges();

    const s = overlayContainerElement.querySelector("span");
    const button = overlayContainerElement.querySelector("button");

    expect(s.textContent).toBe("Are you sure you want to continue?");
    expect(button.textContent).toBe(" Cancel ");
  });
});

// fdescribe("MatConfirmDialogComponent", () => {
//   let component: MatConfirmDialogComponent;
//   let fixture: ComponentFixture<MatConfirmDialogComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [MatConfirmDialogComponent],
//       imports: [MatDialogModule],
//       providers: [{ provide: MAT_DIALOG_DATA }, { provide: MatDialogRef }]
//     }).compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(MatConfirmDialogComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it("should create a dialog box", async(() => {
//     expect(component.data.message).toBeTruthy();
//   }));
// });
