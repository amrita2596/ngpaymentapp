import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { MatDialogModule } from "@angular/material/dialog";
import { UserService } from "../user.service";
import { HttpClientModule } from "@angular/common/http";
import { PayMoneyComponent } from "./pay-money.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

describe("PayMoneyComponent", () => {
  let component: PayMoneyComponent;
  let fixture: ComponentFixture<PayMoneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PayMoneyComponent],
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        HttpClientModule
      ],
      providers: [UserService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayMoneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create the app", () => {
    expect(component).toBeDefined();
  });
});
