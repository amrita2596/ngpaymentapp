import { Component, OnInit, Inject } from "@angular/core";
import { UserService } from "../user.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";

@Component({
  selector: "app-pay-money",
  templateUrl: "./pay-money.component.html",
  styleUrls: ["./pay-money.component.css"]
})
export class PayMoneyComponent implements OnInit {
  payForm: FormGroup;
  sender;
  wallet;
  email;
  userData: any;
  submitted = false;

  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.show();
    this.payForm = this.formBuilder.group({
      Receiver: ["", [Validators.required, Validators.email]],
      Amount: [
        "",
        [Validators.required, Validators.max(20000), Validators.min(1)]
      ]
    });
  }
  get f() {
    return this.payForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    this.wallet = JSON.parse(this.storage.get("wallet"));
    // stop here if amount is invalid
    if (this.payForm.invalid) {
      return;
    } else if (this.payForm.value.Amount > this.wallet) {
      window.alert("insufficient balance in wallet");
      return;
    } else {
      this.userService
        .openConfirmDialog("Are you sure you want to continue?")
        .afterClosed()
        .subscribe(res => {
          if (res) {
            this.sender = JSON.parse(this.storage.get("email"));
            this.userService
              .pay({
                Sender: this.sender,
                Amount: this.payForm.value.Amount,
                Receiver: this.payForm.value.Receiver
              })
              .subscribe(
                data => {
                  this.userData = data;
                  this.storage.set("user", JSON.stringify(this.userData));
                  this.storage.set(
                    "email",
                    JSON.stringify(this.userData.Email)
                  );
                  this.storage.set(
                    "wallet",
                    JSON.stringify(this.userData.Wallet)
                  );
                  this.userService
                    .openNotifyDialog("Payment successful !")
                    .afterClosed();
                  this.payForm.reset();
                },
                error => {
                  this.userService
                    .openNotifyDialog("Payment failed !")
                    .afterClosed();
                  this.payForm.reset();
                }
              );
          }
        });
    }
  }
}
