import { TestBed, async } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { TopBarComponent } from "../app/top-bar/top-bar.component";
import { RouterTestingModule } from "@angular/router/testing";
import { UserService } from "./user.service";
import { HttpClientModule } from "@angular/common/http";
import { MatDialogModule } from "@angular/material/dialog";

describe("AppComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule, MatDialogModule],
      declarations: [AppComponent, TopBarComponent],
      providers: [UserService]
    }).compileComponents();
  }));

  it("should create the app", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it("should have as title 'Epay'", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual("Epay");
  });
});
