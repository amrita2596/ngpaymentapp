import { TestBed } from "@angular/core/testing";
import { HttpClientModule } from "@angular/common/http";
import { UserService } from "./user.service";
import { MatDialogModule } from "@angular/material/dialog";

describe("UserService", () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService],
      imports: [HttpClientModule, MatDialogModule]
    });

    service = TestBed.get(UserService);

    let store = { name: "amrita" };
    const mockLocalStorage = {
      clear: () => {
        store = { name: "" };
      },
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      }
    };
    spyOn(localStorage, "clear").and.callFake(mockLocalStorage.clear);
  });

  it("should be created", () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it("should make visible false when hide is called", () => {
    const service: UserService = TestBed.get(UserService);
    expect(service.hide()).toBe(false);
  });

  it("should make visible true when show is called", () => {
    const service: UserService = TestBed.get(UserService);
    expect(service.show()).toBe(true);
  });

  it("should clear local storage when logout service is called", () => {
    localStorage.clear();
    expect(localStorage.getItem("name")).toEqual(null);
  });
});
