import { Injectable, Inject } from "@angular/core";
import { MatConfirmDialogComponent } from "./mat-confirm-dialog/mat-confirm-dialog.component";
import { MatDialog } from "@angular/material";
import { MatNotifyDialogComponent } from "./mat-notify-dialog/mat-notify-dialog.component";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";

import { User } from "./models/user";
import { Transaction } from "./models/transaction";

@Injectable()
export class UserService {
  visible: boolean;
  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private http: HttpClient,
    private dialog: MatDialog
  ) {}

  register(user: User) {
    return this.http.post("http://localhost:4000/app/register", user);
  }

  login(user: { Email: string; Password: string }) {
    return this.http.post("http://localhost:4000/app/login", user);
  }

  add(data: { Sender: Transaction; Amount: Transaction }) {
    return this.http.post("http://localhost:4000/app/addMoney", data);
  }

  pay(data: {
    Sender: Transaction;
    Amount: Transaction;
    Receiver: Transaction;
  }) {
    return this.http.post("http://localhost:4000/app/payMoney", data);
  }

  history(data: { Email: String }) {
    return this.http.post("http://localhost:4000/app/transactions", data);
  }

  logout() {
    this.storage.clear();
  }

  hide() {
    return (this.visible = false);
  }

  show() {
    return (this.visible = true);
  }

  openConfirmDialog(msg) {
    return this.dialog.open(MatConfirmDialogComponent, {
      width: "390px",
      panelClass: "confirm-dialog-container",
      disableClose: true,
      data: {
        message: msg
      }
    });
  }

  openNotifyDialog(msg) {
    return this.dialog.open(MatNotifyDialogComponent, {
      width: "390px",
      panelClass: "notify-dialog-container",
      position: { top: "20px" },
      disableClose: true,
      data: {
        message: msg
      }
    });
  }
}
