import { Component, OnInit, Inject } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { UserService } from "../user.service";

@Component({
  selector: "app-wallet",
  templateUrl: "./wallet.component.html",
  styleUrls: ["./wallet.component.css"]
})
export class WalletComponent implements OnInit {
  wallet;

  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.show();
    this.wallet = this.storage.get("wallet");
  }
}
