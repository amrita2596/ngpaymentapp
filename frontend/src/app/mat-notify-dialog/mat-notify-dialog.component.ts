import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

@Component({
  selector: "app-mat-notify-dialog",
  templateUrl: "./mat-notify-dialog.component.html",
  styleUrls: ["./mat-notify-dialog.component.css"]
})
export class MatNotifyDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<MatNotifyDialogComponent>
  ) {}

  ngOnInit() {}

  closeDialog() {
    this.dialogRef.close(false);
  }
}
