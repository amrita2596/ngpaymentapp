import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { UserService } from "../user.service";
import { TransactionsComponent } from "./transactions.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { MatDialogModule } from "@angular/material/dialog";
import { HttpClientModule } from "@angular/common/http";

class MockUserService {
  history() {}
}

describe("TransactionsComponent", () => {
  let component: TransactionsComponent;
  let fixture: ComponentFixture<TransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatDialogModule,
        HttpClientModule
      ],
      declarations: [TransactionsComponent],
      providers: [UserService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create the app", () => {
    expect(component).toBeDefined();
  });

  // it("gets the transactions", () => {
  //   const userService = fixture.debugElement.injector.get(UserService);
  //   spyOn(userService, "history");
  //component.
  //});
});
