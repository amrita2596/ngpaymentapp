import { Component, OnInit, Inject } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { UserService } from "../user.service";

@Component({
  selector: "app-transactions",
  templateUrl: "./transactions.component.html",
  styleUrls: ["./transactions.component.css"]
})
export class TransactionsComponent implements OnInit {
  email;
  transactions;
  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.show();
    this.email = JSON.parse(this.storage.get("email"));
    this.userService.history({ Email: this.email }).subscribe(data => {
      this.transactions = data;
      console.log(this.transactions);
    });
  }
}
