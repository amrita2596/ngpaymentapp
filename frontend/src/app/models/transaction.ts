export class Transaction {
  Sender: {
    Email: {
      type: String;
      required: true;
    };
    TransactionType: {
      type: String;
    };
  };
  Amount: Number;
  Receiver: {
    Name: {
      type: String;
    };
    Email: {
      type: String;
    };
    MobileNumber: {
      type: String;
    };
    TransactionType: {
      type: String;
    };
  };
}
