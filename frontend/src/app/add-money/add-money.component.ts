import { Component, OnInit, Inject } from "@angular/core";
import { UserService } from "../user.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";

@Component({
  selector: "app-add-money",
  templateUrl: "./add-money.component.html",
  styleUrls: ["./add-money.component.css"]
})
export class AddMoneyComponent implements OnInit {
  sender;
  wallet;
  email;
  userData: any;
  addForm: FormGroup;
  submitted = false;

  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.show();
    this.addForm = this.formBuilder.group({
      Amount: [
        "",
        [Validators.required, Validators.max(20000), Validators.min(1)]
      ]
    });
  }
  get f() {
    return this.addForm.controls;
  }
  onSubmit() {
    this.submitted = true;

    // stop here if amount is invalid
    if (this.addForm.invalid) {
      return;
    } else {
      this.userService
        .openConfirmDialog("Are you sure you want to continue?")
        .afterClosed()
        .subscribe(res => {
          if (res) {
            this.sender = JSON.parse(this.storage.get("email"));
            this.userService
              .add({ Sender: this.sender, Amount: this.addForm.value.Amount })
              .subscribe(
                data => {
                  this.userData = data;
                  this.storage.set("user", JSON.stringify(this.userData));
                  this.storage.set(
                    "email",
                    JSON.stringify(this.userData.Email)
                  );
                  this.storage.set(
                    "wallet",
                    JSON.stringify(this.userData.Wallet)
                  );
                  this.userService
                    .openNotifyDialog("Money added to wallet !")
                    .afterClosed();
                  this.addForm.reset();
                },
                error => {
                  this.userService
                    .openNotifyDialog("Money can't be added")
                    .afterClosed();
                  this.addForm.reset();
                }
              );
          }
        });
    }
  }
}
