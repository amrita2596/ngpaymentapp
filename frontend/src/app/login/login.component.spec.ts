import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync
} from "@angular/core/testing";
import { Location } from "@angular/common";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { Router } from "@angular/router";
import { MatDialogModule } from "@angular/material/dialog";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { appRoutes } from "../app.routing";
import { HomeComponent } from "../home/home.component";
import { RegisterComponent } from "../register/register.component";
import { WalletComponent } from "../wallet/wallet.component";
import { AddMoneyComponent } from "../add-money/add-money.component";
import { PayMoneyComponent } from "../pay-money/pay-money.component";
import { TransactionsComponent } from "../transactions/transactions.component";
import { LoginComponent } from "./login.component";
import { UserService } from "../user.service";

describe("LoginComponent", () => {
  let userService: UserService;
  let httpMock: HttpTestingController;
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let location: Location;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent,
        AddMoneyComponent,
        PayMoneyComponent,
        HomeComponent,
        RegisterComponent,
        TransactionsComponent,
        WalletComponent
      ],
      imports: [
        RouterTestingModule.withRoutes(appRoutes),
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule
      ],
      providers: [UserService]
    }).compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.get(Router);
    location = TestBed.get(Location);

    fixture = TestBed.createComponent(LoginComponent);
    router.initialNavigation();
    component = fixture.componentInstance;
    userService = TestBed.get(UserService);
    httpMock = TestBed.get(HttpTestingController);
    fixture.detectChanges();

    // var store = {};
    //   const mockLocalStorage = {
    //     getItem: (key: string): string => {
    //       return key in store ? store[key] : null;
    //     },
    //     setItem: (key: string, value: string) => {
    //       store[key] = <string>value;
    //     }
    //   };
    //   spyOn(localStorage, "getItem").and.callFake(mockLocalStorage.getItem);
    //   spyOn(localStorage, "setItem").and.callFake(mockLocalStorage.setItem);
  });
  afterEach(() => {
    httpMock.verify();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should post user to the api via post method", () => {
    const userStub = {
      Email: "string",
      Password: "string"
    };

    const resStub = {
      Email: "string",
      Password: "string"
    };

    userService.login(userStub).subscribe(res => {
      expect(res).toEqual(resStub);
    });
    const req = httpMock.expectOne("http://localhost:4000/app/login");
    expect(req.request.method).toBe("POST");
    req.flush(userStub);
  });

  // it("should set name in localStorage", () => {
  //   localStorage.setItem("name", "amrita");
  //   expect(localStorage.getItem("name")).toBe("amrita");
  // });

  it("should navigate to home page on submit", fakeAsync(() => {
    component.onSubmit();
    router.navigate(["/app/home"]).then(() => {
      expect(location.path()).toBe("/app/home");
    });
  }));
});
