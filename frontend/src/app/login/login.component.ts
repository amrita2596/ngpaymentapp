import { Component, OnInit, Inject } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../user.service";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
@Component({ templateUrl: "login.component.html" })
export class LoginComponent implements OnInit {
  email;
  wallet;
  userData: any;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.hide();
    this.loginForm = this.formBuilder.group({
      Email: ["", [Validators.required, Validators.email]],
      Password: ["", Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.userService.login(this.loginForm.value).subscribe(
      data => {
        this.userData = data;
        // this.storage.set("user", JSON.stringify(this.userData));
        // this.storage.set("email", JSON.stringify(this.userData.Email));
        // this.storage.set("wallet", JSON.stringify(this.userData.Wallet));
        this.storage.set("access_token", this.userData.token);
        this.router.navigate(["/app/home"]);
        return true;
      },
      error => {
        this.userService
          .openNotifyDialog("Incorrect email and password !")
          .afterClosed();
        this.loading = false;
        this.loginForm.reset();
      }
    );
  }
}
