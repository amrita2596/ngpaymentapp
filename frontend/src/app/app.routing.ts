import { Routes, RouterModule } from "@angular/router";

import { RegisterComponent } from "./register/register.component";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { WalletComponent } from "./wallet/wallet.component";
import { AddMoneyComponent } from "./add-money/add-money.component";
import { PayMoneyComponent } from "./pay-money/pay-money.component";
import { TransactionsComponent } from "./transactions/transactions.component";
import { AuthGuard } from "./auth.guard";

export const appRoutes: Routes = [
  { path: "app/register", component: RegisterComponent },
  { path: "app/login", component: LoginComponent },
  { path: "app/home", component: HomeComponent, canActivate: [AuthGuard] },
  { path: "app/wallet", component: WalletComponent, canActivate: [AuthGuard] },
  {
    path: "app/addMoney",
    component: AddMoneyComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "app/payMoney",
    component: PayMoneyComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "app/transactions",
    component: TransactionsComponent,
    canActivate: [AuthGuard]
  }
];

export const routing = RouterModule.forRoot(appRoutes);
