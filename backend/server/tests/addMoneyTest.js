

var mocha = require('mocha');
var expect = require('chai').expect;
var chai = require('chai');
var sinon = require('sinon');
var addMoney = require('../controllers/addMoney');
var stub = sinon.stub();
var User = require("../models/user");
var Transaction = require("../models/transaction");

describe('Add Money test', function () {
    
    var res = {
        send: function (data) { console.log('----', data); }
    };

    var a = {
        _id: "12345_id",
        Name: "Pratik",
        Email: "pratik@gmail.com",
        Password: "qwdfgh",
        Wallet: 0
    }

    let response;

    beforeEach(function () {
        sinon.stub(User, 'findOne');
        sinon.stub(User, 'updateOne');
        response = sinon.spy(res, 'send');
    });


    afterEach(function () {
        User.findOne.restore();
        User.updateOne.restore();
        res.send.restore();
    });

    it('should add Amount and send response', (done) => {

        sinon.stub(Transaction.prototype, 'save');
        Transaction.prototype.save.resolves({});

        var expectedModels = [a];
        User.findOne.resolves(expectedModels[0]);
        User.updateOne.resolves({ n: 1, nModified: 1 });
        var req = {
            body: {
                Sender: '',
                Receiver: 'pratik@gmail.com',
                Amount: 20
            }
        };
        addMoney.addition(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

    it('should throw an errorr if Amount is more than 20000 ', (done) => {
        var req = {
            body: {
                Sender: '',
                Receiver: 'pratik@gmail.com',
                Amount: 20000000
            }
        };
        addMoney.addition(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

    it('should throw an errorr if Amount is null ', (done) => {
        var req = {
            body: {
                Sender: '',
                Receiver: 'pratik@gmail.com',
                Amount: null
            }
        };
        addMoney.addition(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

    it('should throw an errorr if Amount is negative ', (done) => {
        var req = {
            body: {
                Sender: '',
                Receiver: 'pratik@gmail.com',
                Amount: -1
            }
        };
        addMoney.addition(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });
  
    it('should throw an errorr if fieldsare unavailable ', (done) => {
        var req = {
            body: {
              
            }
        };
        addMoney.addition(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

});