
var mocha = require('mocha');
var expect = require('chai').expect;
var chai = require('chai');
var sinon = require('sinon');
var payMoney = require('../controllers/payMoney');
var stub = sinon.stub();
var User = require("../models/user");
var Transaction = require("../models/transaction");

describe('Pay Money test', function () {
    
    var res = {
        send: function (data) { console.log('----', data); }
    };

    var a = {
        _id: "12345_id",
        Name: "Pratik",
        Email: "pratik@gmail.com",
        Password: "qwdfgh",
        Wallet: 1000
    }
    var b = {
        _id: "123545_id",
        Name: "Pratik",
        Email: "mahore@gmail.com",
        Password: "qwdfgh",
        Wallet: 0
    }
    let response;

    beforeEach(function () {
        sinon.stub(User, 'findOne');
        sinon.stub(User, 'updateOne');
        response = sinon.spy(res, 'send');
    });


    afterEach(function () {
        User.findOne.restore();
        User.updateOne.restore();
        res.send.restore();
        
    });

    it('should send Money', (done) => {

        var expectedModelsSender = [a];
      //  var expectedModelsReceiver = [b];
        User.findOne.resolves(expectedModelsSender[0]);
        User.updateOne.resolves({ n: 1, nModified: 1 });
      
        var req = {
            body: {
                Sender: 'pratik@gmail.com',
                Receiver: 'mahore@gmail.com',
                Amount: 200
            }
        };
        payMoney.pay(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

    it('should throw an errorr if Amount is more than 10000 ', (done) => {
        var req = {
            body: {
                Sender: '',
                Receiver: 'mahore@gmail.com',
                Amount: 20000000
            }
        };
        payMoney.pay(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

    it('should throw an errorr if Amount is null', (done) => {
        var req = {
            body: {
                Sender: '',
                Receiver: 'mahore@gmail.com',
                Amount: null
            }
        };
        payMoney.pay(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

    it('should throw an errorr if Amount is negative ', (done) => {
        var req = {
            body: {
                Sender: '',
                Receiver: 'mahore@gmail.com',
                Amount: -1
            }
        };
        payMoney.pay(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

    it('should throw an errorr if receiver is null', (done) => {
        var req = {
            body: {
                Sender: '',
                Receiver: null,
                Amount: 25
            }
        };
        payMoney.pay(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });
    it('should throw en error if user not found', (done) => {

        var expectedModelsSender = [a];
        User.findOne.rejects(expectedModelsSender[0]);
  //last cath block
    
        var req = {
            body: {
                Sender: 'pratik@gmail.com',
                Receiver: 'mahore@gmail.com',
                Amount: 200
            }
        };
        payMoney.pay(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });
  
    it('should throw en error if wallet is lesser than amount', (done) => {
        //var expectedModelsReceiver = [b];
        var expectedModelsSender = [a];
        User.findOne.resolves(expectedModelsSender[0]);
    
        var req = {
            body: {
                Sender: 'pratik@gmail.com',
                Receiver: 'mahore@gmail.com',
                Amount: 1001
            }
        };
        payMoney.pay(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

});