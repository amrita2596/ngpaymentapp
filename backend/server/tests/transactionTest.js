

var mocha = require('mocha');
var expect = require('chai').expect;
var chai = require('chai');
var sinon = require('sinon');
var Trans = require('../controllers/transaction');
var stub = sinon.stub();
var User = require('../models/user');
var Transaction = require("../models/transaction");

describe('Transaction API test', function () {
    
    var res = {
        send: function (data) { console.log('----', data); }
    };

    var a = {
        _id: "12345_id",
        Name: "Pratik",
        Email: "pratik@gmail.com",
        Password: "qwdfgh",
        Wallet: 100
    }

  
    let response;

    beforeEach(function () {
        sinon.stub(User, 'findOne');
        sinon.stub(Transaction, 'find');
        response = sinon.spy(res, 'send');
    });


    afterEach(function () {
        User.findOne.restore();
        Transaction.find.restore();
        res.send.restore();
    });

    it('should send transaction', (done) => {
        var transactData ={
            Sender:"pratik@gmail.com",
            Receiver:"karan2@gmail.com",
            Amount :20,
            Type:"Credit"
          }
       
        var expectedModels = [a];
        var expectedTransaction=[transactData];
        User.findOne.resolves(expectedModels[0]);
        Transaction.find.resolves(expectedTransaction[0]);
        //Transaction.find.resolves()
        var req = {
            body: {
             Email:'pratik@gmail.com'
            }
        };
        Trans.history(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

    it('should not send transaction', (done) => {
        var transactData ={};
          
       
        var expectedModels = [a];
        var expectedTransaction=[transactData];
        User.findOne.resolves(expectedModels[0]);
        Transaction.find.resolves(expectedTransaction[0]);
        //Transaction.find.resolves()
        var req = {
            body: {
             Email:'pratik@gmail.com'
            }
        };
        Trans.history(req, res);
        Promise.resolve(() => {
            sinon.assert.called(response);
        }).then(() => done());

    });

    // it('should get transaction', (done) => {
    
    //     var expectedTransaction=[transactData];
    //     Transaction.find.resolves(expectedTransaction[0]);
    //     Transaction.find.resolves()
    //     var req = {
    //         body: {
    //          Email:'pratik@gmail.com'
    //         }
    //     };
    //     Trans.getHistory(req, res);
    //     Promise.resolve(() => {
    //         sinon.assert.called(response);
    //     }).then(() => done());

    // });

    // it('should send data', (done) => {
    //     var expectedModels = [a];
    //     User.findOne.resolves(expectedModels[0]);
    //     var req = {
    //         body: {
    //          Email:'pratik@gmail.com'
    //         }
    //     };
    //     Trans.history(req, res);
    //     Promise.resolve(() => {
    //         sinon.assert.called(response);
    //     }).then(() => done());

    // });

    // it('should throw an errorr ', (done) => {
    //     var req = {
    //         body: {
    //             Sender: '',
    //             Receiver: 'pratik@gmail.com',
    //             Amount: 20000000
    //         }
    //     };
    //     addMoney.addition(req, res);
    //     Promise.resolve(() => {
    //         sinon.assert.called(response);
    //     }).then(() => done());

    // });

    // it('should throw an errorr ', (done) => {
    //     var req = {
    //         body: {
    //             Sender: '',
    //             Receiver: 'pratik@gmail.com',
    //             Amount: null
    //         }
    //     };
    //     addMoney.addition(req, res);
    //     Promise.resolve(() => {
    //         sinon.assert.called(response);
    //     }).then(() => done());

    // });

    // it('should throw an errorr ', (done) => {
    //     var req = {
    //         body: {
    //             Sender: '',
    //             Receiver: 'pratik@gmail.com',
    //             Amount: -1
    //         }
    //     };
    //     addMoney.addition(req, res);
    //     Promise.resolve(() => {
    //         sinon.assert.called(response);
    //     }).then(() => done());

    // });

});