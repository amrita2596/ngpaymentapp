var mocha = require("mocha"),
  chai = require("chai"),
  sinon = require("sinon"),
  bcrypt = require("bcrypt");
var testObject = require("../controllers/register");
var UserModel = require("../models/user");

describe("GetUser", function() {
  beforeEach(function() {
    sinon.stub(UserModel, "find");
  });

  afterEach(function() {
    UserModel.find.restore();
  });

  it("should return all users", function() {
    var a = {
      _id: "some_id",
      Name: "abc",
      Email: "abc@gmail.com",
      MobileNumber: "1234567890",
      Password: "Password",
      Wallet: "0"
    };
    var expectedModels = [a];
    UserModel.find.yields(null, expectedModels);
    var req = {};
    var res = {
      send: sinon.stub()
    };

    testObject.getUser(req, res);
    expectedModels[0].should.include.keys(
      "_id",
      "Name",
      "Email",
      "MobileNumber",
      "Password",
      "Wallet"
    );
    sinon.assert.calledWith(res.send, expectedModels);
  });

  it("should return error if users can't be found", function() {
    var expectedError = "unable to retrieve data";
    UserModel.find.yields(expectedError, null);
    var req = {};
    var res = {
      send: sinon.stub(),
      status: sinon.stub()
    };

    testObject.getUser(req, res);
    sinon.assert.calledWith(res.send, expectedError);
  });
});

describe("addUser", function() {
  var req = {
    body: {
      Name: "text",
      Email: "text@gmail.com",
      MobileNumber: 4535325677,
      Password: "text123"
    }
  };
  var res = {
    send: function(data) {}
  };
  var response;
  beforeEach(function() {
    sinon.stub(bcrypt, "hash");
    sinon.stub(UserModel.prototype, "save");
    response = sinon.spy(res, "send");
  });

  afterEach(function() {
    UserModel.prototype.save.restore();
    bcrypt.hash.restore();
    res.send.restore();
  });

  it("should save user if all fields are filled", function(done) {
    bcrypt.hash.resolves({});
    var expectedResponse = "success message";
    UserModel.prototype.save.resolves(expectedResponse);
    testObject.addUser(req, res);
    Promise.resolve(() => {
      sinon.assert.called(response);
    }).then(() => done());
  });

  it("should throw an error if name is not filled", function(done) {
    var req = {
      body: {
        Email: "text@gmail.com",
        MobileNumber: 4535325677,
        Password: "text123"
      }
    };
    testObject.addUser(req, res);
    Promise.resolve(() => {
      sinon.assert.called(response);
    }).then(() => done());
  });

  it("should throw an error if email is not filled", function(done) {
    var req = {
      body: {
        Name: "text",
        MobileNumber: 4535325677,
        Password: "text123"
      }
    };
    testObject.addUser(req, res);
    Promise.resolve(() => {
      sinon.assert.called(response);
    }).then(() => done());
  });

  it("should throw an error if mobile number is not filled", function(done) {
    var req = {
      body: {
        Name: "text",
        Email: "text@gmail.com",
        Password: "text123"
      }
    };
    testObject.addUser(req, res);
    Promise.resolve(() => {
      sinon.assert.called(response);
    }).then(() => done());
  });

  it("should throw an error if password is not filled", function(done) {
    var req = {
      body: {
        Name: "text",
        Email: "text@gmail.com",
        MobileNumber: 4535325677
      }
    };
    testObject.addUser(req, res);
    Promise.resolve(() => {
      sinon.assert.called(response);
    }).then(() => done());
  });
});
