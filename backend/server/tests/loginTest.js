var mocha = require("mocha"),
  chai = require("chai"),
  sinon = require("sinon"),
  bcrypt = require("bcrypt");
 // should=require("should");

var testObject = require("../controllers/login");
var UserModel = require("../models/user");

describe("login user", function() {
  var req = {
    body: {
      Email: "abc@gmail.com",
      Password: "abcdef"
    }
  };
  var res = {
    send: function(data) {}
  };
  var a = {
    _id: "some_id",
    Name: "text",
    Email: "text@gmail.com",
    MobileNumber: 4535325677,
    Password: "text123",
    Wallet: 8768
  };
  var response;
  beforeEach(function() {
    sinon.stub(UserModel, "findOne");
    sinon.stub(bcrypt, "compare");
    response = sinon.spy(res, "send");
  });

  afterEach(function() {
    UserModel.findOne.restore();
    res.send.restore();
    bcrypt.compare.restore();
  });

  it("should return login user if found", function(done) {
    bcrypt.compare.resolves({});
    var expectedModels = [a];
    UserModel.findOne.resolves(expectedModels[0]);
    testObject.userLogin(req, res);
    expectedModels[0].should.include.keys(
      "_id",
      "Name",
      "Email",
      "MobileNumber",
      "Password",
      "Wallet"
    );
    Promise.resolve(() => {
      sinon.assert.called(response);
    }).then(() => done());
  });

  it("should return error if user not found", function(done) {
    bcrypt.compare.resolves({});
    var expectedError = "error";
    UserModel.findOne.rejects(expectedError);
    testObject.userLogin(req, res);
    Promise.reject(() => {
      sinon.assert.called(response);
    }).then(() => done());
  });
});
