const express = require("express"),
  bodyParser = require("body-parser"),
  cors = require("cors"),
  mongoose = require("mongoose");
bcrypt = require("bcrypt");
port = process.env.PORT || 4000;
const jwt = require("jsonwebtoken");
const expressJwt = require("express-jwt");

// accessing the all the methods of express in the object "app"
const app = express();

const router = require("./routes/router");

const db = require("./config/db");
db();

app.get("/", (req, res) => {
  return res.send("hello");
});

app.use(bodyParser.urlencoded({ extended: false }));

//it tells the system that we want json to be used
app.use(bodyParser.json());

app.use(cors());
app.use("/app", router);

//app starts a server and listens on port 4000 for connections
const server = app.listen(port, function() {
  console.log("Listening on port " + port);
});
module.exports = server;
