const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TransactionSchema = new Schema(
  {
    Sender: {
      Email: {
        type: String,
        required: true
      },
      TransactionType: {
        type: String
      }
    },

    Receiver: {
      Name: {
        type: String
      },
      Email: {
        type: String,
        required: true
      },
      MobileNumber: {
        type: String
      },
      TransactionType: {
        type: String
      }
    },

    Amount: { type: Number, min: 0, max: 20000 },
    Date: {
      type: Date,
      default: Date.now
    }
  },
  { collection: "transactiondata" }
);

module.exports = mongoose.model("TransactionSchema", TransactionSchema);
