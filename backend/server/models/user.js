var validateEmail = function(Email) {
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(Email);
};

var validateMobileNumber = function(MobileNumber) {
  var m = /^\d{10}$/;
  return m.test(MobileNumber);
};

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Define collection and schema for userModel
let UserModel = new Schema(
  {
    Name: {
      type: String,
      maxlength: 25,
      required: true
    },
    Email: {
      type: String,
      required: true,
      trim: true,
      lowercase: true,
      unique: true,
      validate: [validateEmail, "Please fill a valid email address"],
      match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    MobileNumber: {
      type: Number,
      required: true,
      unique: true,
      validate: [validateMobileNumber, "Please fill a valid Mobile number"],
      match: [/^\d{10}$/]
    },
    Password: {
      type: String,
      required: true
    },
    Wallet: {
      type: Number,
      multipleOf: 1.0,
      default: 0.0
    }
  },
  {
    collection: "userdata"
  }
);

module.exports = mongoose.model("UserModel", UserModel);
