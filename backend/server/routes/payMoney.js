const express = require("express");
const Router = express.Router();
const payMoney = require("../controllers/payMoney");

Router.post("/", payMoney.pay);

module.exports = Router;
