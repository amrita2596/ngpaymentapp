const express = require("express");
const Router = express.Router();

const reg = require("../controllers/register");
Router.post("/register", reg.addUser);
Router.get("/", reg.getUser);

module.exports = Router;
