const express = require("express");
const Router = express.Router();

const userLog = require("../controllers/login");

Router.post("/", userLog.userLogin);

module.exports = Router;
