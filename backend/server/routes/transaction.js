const express = require("express");
const Router = express.Router();

const trns = require("../controllers/transaction");
Router.post("/", trns.history);

module.exports = Router;
