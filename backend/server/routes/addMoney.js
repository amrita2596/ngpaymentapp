const express = require("express");
const Router = express.Router();

const add = require("../controllers/addMoney");
Router.post("/", add.addition);

module.exports = Router;
