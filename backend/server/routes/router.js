const express = require("express");
const Router = express.Router();

const register = require("./register");
const payMoney = require("./payMoney");
const loginUser = require("./login");
const addMoney = require("./addMoney");
const transaction = require("./transaction");

Router.use("/register", register);
Router.use("/", register);
Router.use("/payMoney", payMoney);
Router.use("/login", loginUser);
Router.use("/addMoney", addMoney);
Router.use("/transactions", transaction);

module.exports = Router;
