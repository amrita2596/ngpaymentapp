// Require UserModel model in our routes module
let UserModel = require("../models/user");
let TransactionSchema = require("../models/transaction");

//payment api
exports.pay = (req, res) => {
  let requestObject = req.body;
  if (req.body.Receiver === null) {
    return res.send({
      message: "Receiver can not be null!"
    });
  } else if (req.body.Amount === null) {
    return res.send({
      message: "Amount should not be negative !"
    });
  } else if (req.body.Amount <= 0) {
    return res.send({
      message: "Amount should not be negative !"
    });
  } else if (req.body.Amount > 10000) {
    return res.send({
      message: "Amount should less than 10,001!"
    });
  } else {
    UserModel.findOne({ Email: req.body.Sender })
      .then(senderData => {
        if (senderData.Wallet >= req.body.Amount) {
          UserModel.findOne({ Email: req.body.Receiver })
            .then(receiverData => {
              //if receiver exists then perform payment
              if (receiverData) {
                let history = new TransactionSchema({
                  Sender: { Email: req.body.Sender, TransactionType: "debit" },
                  Amount: req.body.Amount,
                  Receiver: {
                    Email: req.body.Receiver,
                    TransactionType: "credit"
                  }
                });

                let registeredUser;
                //saving data in transactions database
                history
                  .save()
                  .then(trans => {
                    return UserModel.findOne({ Email: req.body.Sender });
                  })
                  //accessing senders wallet and updating it
                  .then(user => {
                    registeredUser = user;
                    const updateWallet =
                      registeredUser.Wallet - requestObject.Amount;
                    return UserModel.updateOne(
                      { _id: registeredUser.id },
                      { $set: { Wallet: updateWallet } }
                    );
                  })
                  //taking the updated senders data and returning it in response
                  .then(updatedCount => {
                    const senderEmail = requestObject.Sender;
                    return UserModel.findOne({ Email: senderEmail });
                  })
                  .then(updated => {
                    newUpdated = updated;
                    return res.send(newUpdated);
                  })
                  //accessing receivers wallet and updating it
                  .then(trans => {
                    return UserModel.findOne({ Email: req.body.Receiver });
                  })
                  .then(user => {
                    registeredUser = user;
                    const updateWallet =
                      registeredUser.Wallet + requestObject.Amount;
                    return UserModel.updateOne(
                      { _id: registeredUser.id },
                      { $set: { Wallet: updateWallet } }
                    );
                  });
              }
              //  else if (!recieverData) {
              //   return res.send(204, "receiver doesnt exist");
              // } else if (recieverData.Email === req.body.Sender) {
              //   return res.send(201, "Can't send yourself");
              // } else if (req.body.Amount > 10000 || req.body.Amount < 1) {
              //   return res.send(202, "Cant pay more than 10000 ");
              // } else {
              //   return res.send(err);
              // }
            })
            .catch(err => {
              return res.send({
                message: "Receiver Doesn't exists"
              });
            });
        } else
          return res.send({
            message: "Sender wallet is less"
          });
      })
      .catch(err => {
        return res.send({
          message: "Sender doesn't exists"
        });
      });
  }
};
