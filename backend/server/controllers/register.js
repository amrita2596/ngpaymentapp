// Require UserModel model in our routes module
var UserModel = require("../models/user");
var bcrypt = require("bcrypt");
// post function to register new user
var BCRYPT_SALT_ROUNDS = 10;

module.exports = { addUser, getUser };

function addUser(req, res) {
  //Getting data of a new user.
  const newUser = new UserModel({
    Name: req.body.Name,
    Email: req.body.Email,
    MobileNumber: req.body.MobileNumber,
    Password: req.body.Password
  });
  if (!newUser.Name) {
    return res.send("name is required");
  }

  if (!newUser.Email) {
    return res.send("email is required");
  }

  if (!newUser.Password) {
    return res.send("password is required");
  }

  if (!newUser.MobileNumber) {
    return res.send("mobile number is required");
  }

  bcrypt
    .hash(newUser.Password, BCRYPT_SALT_ROUNDS)
    .then(function(hashedPassword) {
      newUser.Password = hashedPassword;
      newUser.save();
      return res.send("user is registered");
    })
    .catch(error => {
      res.status(400);
      res.send("error registering user", error);
    });
}

//get function to see registered users
function getUser(req, res) {
  UserModel.find({}, function(error, users) {
    if (error) {
      res.status(400);
      res.send("unable to retrieve data");
    } else {
      res.send(users);
    }
  });
}

return { addUser, getUser };
