// Require UserModel model in our routes module
const UserModel = require("../models/user");
const jwt = require("jsonwebtoken");
const expressJwt = require("express-jwt");
const bcrypt = require("bcrypt");

module.exports = {
  userLogin
};

function userLogin(req, res) {
  var Pass = req.body.Password;
  //finding user with given email
  UserModel.findOne({
    Email: req.body.Email
  })
    //if email found, checks for password
    .then(function(user) {
      bcrypt.compare(Pass, user.Password).then(password => {
        if (password === false) {
          return res.status(400).send("incorrect password");
        }
        var token = jwt.sign(
          { userID: user.Email },
          "todo-app-super-shared-secret",
          { expiresIn: "2h" }
        );
        return res.status(200).send({ token });
        //return res.status(200).send(user);
      });
    })
    .catch(function(error) {
      res.status(400).send("user needs to register first");
    });
}
return { userLogin };
