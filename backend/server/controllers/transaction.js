let UserModel = require("../models/user");
let TransactionSchema = require("../models/transaction");

// post function to check for transaction history of a user
exports.history = (req, res) => {
  UserModel.findOne({ Email: req.body.Email }).then(user => {
    let userEmail = user.Email;
    TransactionSchema.find({
      $or: [
        { "Sender.Email": req.body.Email },
        { "Receiver.Email": req.body.Email }
      ]
    })
      .then(transactData => {
        if (transactData === null) {
          return res.send({
            message: "No transaction Found !"
          });
        } else {
          let tmpArray = [];

          transactData.forEach(element => {
            tmpArray.push({
              Sender: element.Sender.Email,
              Receiver: element.Receiver.Email,
              Amount: element.Amount,
              Type:
                req.body.Email === element.Sender.Email
                  ? element.Sender.Email === element.Receiver.Email
                    ? "Added to your wallet!"
                    : "Debit"
                  : "Credit"
            });
          });
          var money = tmpArray.reverse();
          return res.json(money);
        }
      })
      .catch(error => {
        return res.status(400).send("cannot retreive transactions !");
      });
  });
};
