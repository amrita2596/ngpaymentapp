const User = require("../models/user");
const TransactionSchema = require("../models/transaction");
exports.addition = (req, res) => {
  if (req.body.Amount === 0) {
    return res.send({
      message: "Amount can not be null!"
    });
  } else if (req.body.Amount < 0) {
    return res.send({
      message: "Amount should not be negative !"
    });
  } else if (req.body.Amount > 20000) {
    return res.send({
      message: "Amount should less than 20,000!"
    });
  } else {
    let requestObject = req.body;
    let history = new TransactionSchema({
      Sender: {
        Email: req.body.Sender,
        TransactionType: "Added to your wallet!"
      },
      Amount: req.body.Amount,
      Receiver: {
        Email: req.body.Sender,
        TransactionType: "Added to your wallet!"
      }
    });
    let userObject;

    history
      .save()
      .then(trans => {
        const senderEmail = requestObject.Sender;
        return User.findOne({ Email: senderEmail });
      })
      .then(user => {
        userObject = user;
        const updateWallet = (user.Wallet + requestObject.Amount).toFixed(2);
        return User.updateOne(
          { _id: user.id },
          { $set: { Wallet: updateWallet } }
        );
      })
      .then(updatedCount => {
        return User.findOne({ Email: req.body.Sender });
      })
      .then(updated => {
        newUpdated = updated;
        return res.send(newUpdated);
      })
      .catch(err => {
        res.send({
          message: "Amount should less than 20,000!"
        });
      });
  }
};
