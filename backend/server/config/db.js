mongoose = require("mongoose");

// connect the database "posts" running locally on the default port (27017).
module.exports = function db() {
  mongoose.connect("mongodb://localhost:27017/users").then(
    () => {
      console.log("Database is connected");
    },
    err => {
      console.log("Can not connect to the database" + err);
    }
  );
};
