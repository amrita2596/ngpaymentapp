
#E-Pay {Backend}

This  project used Node.js as a platform, express as a web framework and MongoDB as a NoSQL database.
Below you will find some information on how to perform common tasks.

## Table of Contents


-[How to Run Backend](#How-to-run-the-Backend)
- [Installing node](#installing-node)
- [Folder Structure](#folder-structure)
- [Supported Browsers](#supported-browsers)
- [Installing a Dependency](#installing-a-dependency)

#How to run the Backend
- inside "backend" directory run `npm install` to have all npm packages present in package.json file.
- run `node app.js` for running the backend in localhost://4000.
- run `mongod` in another terminal to start the mongo server.


##Installing node

To start building  Node.js applications, the first step is the installation of the node.js framework. The Node.js framework is available for a variety of operating systems right from Windows to Ubuntu and OS X. Once the Node.js framework is installed you can start building your first Node.js applications.
for Ubuntu run `sudo apt install nodejs` similarly do for mongodb and mongoose.

## Folder Structure

After creation, your project should look like this:

```
Backend/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
    mainfest.json
  src/
     app.js
     config/
            DB.js
     controllers/
                addMoney.js
                login.js
                Pay_money.js
                register.js
                transaction.js 
     models/
           Transaction.js
           user.js
     routes/
          addMoney.js
          login.js
          Pay_money.js
          register.js

```

For the project to build, **these files must exist with exact filenames**:

* `public/index.html` is the page template;
* `src/index.js` is the JavaScript entry point.

You can delete or rename the other files.

You may create subdirectories inside `src`. For faster rebuilds, only files inside `src` are processed by Webpack.<br>
You need to **put any JS and CSS files inside `src`**, otherwise Webpack won’t see them.

Only files inside `public` can be used from `public/index.html`.<br>
Read instructions below for using assets from JavaScript and HTML.

You can, however, create more top-level directories.<br>
They will not be included in the production build so you can use them for things like documentation.


## Supported Browsers

By default, the generated project uses the latest version of React.

You can refer [to the React documentation](https://reactjs.org/docs/react-dom.html#browser-support) for more information about supported browsers.


## Installing a Dependency

You can install Dependencies like nodemon , mongoose for this run `npm install nodemon`. 
nodemon module automatically restart the server in case of modification in any files. 












